
package org.easy.tool.web;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.servlet.http.HttpServletResponse;

/**
 * 业务代码枚举
 *
 */
@Getter
@AllArgsConstructor
public enum ResultCode implements IResultCode {

	/**
	 * 操作成功
	 */
	SUCCESS(HttpServletResponse.SC_OK, "操作成功"),

	/**
	 * 业务异常
	 */
	FAILURE(HttpServletResponse.SC_BAD_REQUEST, "业务异常"),

	/**
	 * 请求未授权
	 */
	UN_AUTHORIZED(HttpServletResponse.SC_UNAUTHORIZED, "请求未授权"),

	/**
	 * 客户端请求未授权
	 */
	CLIENT_UN_AUTHORIZED(HttpServletResponse.SC_UNAUTHORIZED, "客户端请求未授权"),

	/**
	 * 404 没找到请求
	 */
	NOT_FOUND(HttpServletResponse.SC_NOT_FOUND, "404 没找到请求"),

	/**
	 * 消息不能读取
	 */
	MSG_NOT_READABLE(HttpServletResponse.SC_BAD_REQUEST, "消息不能读取"),

	/**
	 * 不支持当前请求方法
	 */
	METHOD_NOT_SUPPORTED(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "不支持当前请求方法"),

	/**
	 * 不支持当前媒体类型
	 */
	MEDIA_TYPE_NOT_SUPPORTED(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "不支持当前媒体类型"),

	/**
	 * 请求被拒绝
	 */
	REQ_REJECT(HttpServletResponse.SC_FORBIDDEN, "请求被拒绝"),

	/**
	 * 服务器异常
	 */
	INTERNAL_SERVER_ERROR(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "服务器异常"),

	/**
	 * 缺少必要的请求参数
	 */
	PARAM_MISS(HttpServletResponse.SC_BAD_REQUEST, "缺少必要的请求参数"),

	/**
	 * 请求参数类型错误
	 */
	PARAM_TYPE_ERROR(HttpServletResponse.SC_BAD_REQUEST, "请求参数类型错误"),

	/**
	 * 请求参数绑定错误
	 */
	PARAM_BIND_ERROR(HttpServletResponse.SC_BAD_REQUEST, "请求参数绑定错误"),

	/**
	 * 参数校验失败
	 */
	PARAM_VALID_ERROR(HttpServletResponse.SC_BAD_REQUEST, "参数校验失败"),


	AUTH_USERNAME_NONE( 23001, "请输入账号！"),
	AUTH_PASSWORD_NONE( 23002, "请输入密码！"),
	AUTH_VERIFYCODE_NONE( 23003, "请输入验证码！"),
	AUTH_ACCOUNT_NOTEXISTS( 23004, "账号不存在！"),
	AUTH_CLIENT_NOTEXISTS( 23005, "客户端不存在！"),
	AUTH_CREDENTIAL_ERROR( 23006, "账号或密码错误！"),
	AUTH_LOGIN_APPLYTOKEN_FAIL( 23007, "申请令牌失败！"),
	AUTH_LOGIN_TOKEN_SAVEFAIL( 23008, "存储令牌失败！"),

	INVALID_PARAM( 10003, "非法参数！"),
	UNAUTHENTICATED( 10001, "此操作需要登陆系统！"),
	UNAUTHORISE( 10002, "权限不足，无权操作！"),
	FORBIDDEN( 10003, "请重新获取令牌"),
	SERVER_ERROR( 99999, "抱歉，系统繁忙，请稍后重试！");

	;

	/**
	 * code编码
	 */
	final int code;
	/**
	 * 中文信息描述
	 */
	final String message;


	public static ResultCode getResultCodeByCode(int code){
		for (ResultCode resultCode : ResultCode.values()){
			if (code==resultCode.getCode()){
				return resultCode;
			}
		}
		return null;
	}

}
